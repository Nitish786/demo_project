// import logo from './logo.svg';
import './App.css';
import Routes from './Routes'; // Uncomment this line

function App() {
  return (
    <div className="App">
     <Routes></Routes>
    </div>
  );
}

export default App;
