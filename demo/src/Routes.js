import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'; // Use Routes instead of Switch

// import Dashboard from './component/Dashboard';
import Users from './component/Users'; // Update the import path
import Header from './component/Header';
import Dashboard from './component/Dashboard';

const MyRoutes = () => {
  return (
    <Router>
      <Routes>
      <Route path="/" element={<Header/>} />
      <Route path="/" element={<Dashboard />} />
      <Route path="/users" element={<Users />} />

      </Routes>
    </Router>
  );
};

export default MyRoutes;
