import { render, screen } from '@testing-library/react';
import App from './App';
// import Routes from './Routes';

test('renders learn react link', () => {
  render(<App />);
   // render(<Routes />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
